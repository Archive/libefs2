<SECTION>
<FILE>efs</FILE>
<TITLE>Filesystem Handling</TITLE>
efs_open
efs_open_cb
efs_close
efs_commit
efs_rollback
efs_fsstat
</SECTION>

<SECTION>
<FILE>file</FILE>
<TITLE>File Operations</TITLE>
efs_file_open
efs_file_close
efs_file_seek
efs_file_tell
efs_file_read
efs_file_write
efs_file_trunc
efs_type_set
efs_type_get
efs_stat
efs_erase
</SECTION>

<SECTION>
<FILE>dir</FILE>
<TITLE>Directory Handling</TITLE>
efs_dir_open
efs_dir_close
efs_dir_seek
efs_dir_tell
efs_dir_read
efs_rename
</SECTION>

<SECTION>
<FILE>util</FILE>
<TITLE>Utility Functions</TITLE>
efs_lock_create
efs_lock_remove
efs_lock_check
efs_strerror

</SECTION>

<SECTION>
<FILE>data</FILE>
<TITLE>Constants and Data Structures</TITLE>
EFS_MAXPATHLEN
EFS_FDEL

EFSHeader

EFSFile
EFSDir
EFSDirEntry
EFSStat
EFSFSStat

EFS_DIR
EFS_FILE

EFS_READ
EFS_WRITE
EFS_RDWR
EFS_EXCL
EFS_CREATE
EFS_COMP
EFS_APPEND
EFS_PROT

EFS_SEEK_CUR
EFS_SEEK_SET
EFS_SEEK_END

EFS_FILE_ID
EFS_MAXNAMELEN

EFSResult

</SECTION>

<SECTION>
<FILE>blowfish</FILE>
<TITLE>Blowfish Encryption Functions</TITLE>
BlowfishCTX
blowfish_init
blowfish_encrypt
blowfish_decrypt
</SECTION>

<SECTION>
<FILE>ib1driver</FILE>
<TITLE>IB1 Data Structures</TITLE>
IB1_N_BLOCKS
IB1_NAME_LEN
IB1Header
IB1INode
IB1DirEntry
</SECTION>


<SECTION>
<FILE>ib1unused</FILE>
<TITLE>unused IB1 declarations</TITLE>
ib1_encrypt
ib1_block_free
ib1_inode_unref
IB1_CACHE_SIZE
CLOCK
CUNLOCK
ib1_block_alloc
ib1_cache_flush
ib1_decrypt
ib1_bitmap_free
IB1EFS
ib1_inode_create
efs_driver_ib1
ib1_inode_ref
ib1_inode_bmap
IB1INodeLEntry
file_ops_ib1
IB1_ROOT_INODE
IB1_IMAP_INODE
ib1_cache_touch
IB1_IBCACHE_SIZE
NODEP
IB1Bitmap
super_ops_ib1
ib1_cache_map_clone
IB1File
ib_inode_refcount
IB1CacheEntry
ib1_inode_list_free
ib1_bitmap_init
efs_driver_ib1_enc
IB1IBCEntry
ib1_inode_map
dir_ops_ib1
ib1_cache_map
ib1_block_get_fbc
IB1_ICACHE_SIZE
ib1_inode_trunc
ib1_inode_erase
IB1ICEntry
IB1Dir
</SECTION>
